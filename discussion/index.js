console.log("Hello, World!");

function printName(name){
	console.log("Hello, " + name + "!")
}

printName("John");

printName();

printName(null);

let sampleVariable = "Yui";

printName(sampleVariable);

function checkDivisibilityBy8(num) {
	let remainder = num % 8;
	console.log("The remainder of " + num + " divided by 8 is " + remainder + ".");
	let isDivisibleBy8 = remainder === 0;
	console.log("Is " + num + "divisible by 8?");
	console.log(isDivisibleBy8);
}

checkDivisibilityBy8(64);
checkDivisibilityBy8(28);

// Functions as Arguments

// Functions paramaters can also accept other functions as arguments

function argumentFunction(num) {
	console.log("This function was passed as an argument before the message was printed.");
	console.log(num + 2);
}

function invokeFunction(argumentFunction) {
	argumentFunction(2);
	console.log("This code comes from invokeFunction.");
}

invokeFunction(argumentFunction);

// Multiple Arguments - will correspond to the number of "Parameters" declared in a function in succeeding order. 

function createFullName(firstName, middleName, lastName) {
	console.log(firstName + " " + middleName + " " + lastName);
}

createFullName("Cadan", "Marcus");
createFullName("Garrett", "Galahad", "Noah");
createFullName("Garrett", "Galahad", "Noah", "Hello");

let firstName = "John";
let middleName = "Doe";
let lastName = "Smith";

createFullName(firstName, middleName, lastName);

// The Return Statements - allows us to output a value from a function to be passed tot he line/block of code that invoked/called the function.

// The 'return' statement also stops the execution of the function and any code the return statement will not be executed.

function returnFullName(firstName, middleName, lastName) {
	return firstName + " " + middleName + " " + lastName;
	console.log("This message will not be printed");
}

returnFullName("Jack", "N'", "Jill");
console.log(returnFullName("Dolo", "N'", "Culpa"));

function returnAddress(city, country) {
	let fullAddress = city + ", " + country;
	return fullAddress;
}

console.log(returnAddress("Cebu City", "Philippines"));